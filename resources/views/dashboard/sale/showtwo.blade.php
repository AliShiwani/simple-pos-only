<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Facture | {{ $sale->number_sale }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Theme style -->
    <style>
 #invoice-POS{
  box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
  padding:2mm;
  margin: 0 auto;
  width: 44mm;
  background: #FFF;
  
 }
h1{
  font-size: 1.5em;
  color: #222;
}
h2{font-size: .9em;}
h3{
  font-size: 1.2em;
  font-weight: 300;
  line-height: 2em;
}
p{
  font-size: .7em;
  color: #666;
  line-height: 1.2em;
}
 
#top, #mid,#bot{ /* Targets all id with 'col-' */
  border-bottom: 1px solid #EEE;
}

#top{min-height: 100px;}
#mid{min-height: 80px;} 
#bot{ min-height: 50px;}

#top .logo{
  //float: left;
	height: 60px;
	width: 60px;
	background: url({{config('app.url')}}/uploads/settings/{{ $logo }}) no-repeat;
	background-size: 60px 60px;
}
.clientlogo{
  float: left;
	height: 60px;
	width: 60px;
	background: url(http://michaeltruong.ca/images/client.jpg) no-repeat;
	background-size: 60px 60px;
  border-radius: 50px;
}
.info{
  display: block;
  //float:left;
  margin-left: 0;
}
.title{
  float: right;
}
.title p{text-align: right;} 
table{
  width: 100%;
  border-collapse: collapse;
}
td{
  //padding: 5px 0 5px 15px;
  //border: 1px solid #EEE
}
.tabletitle{
  /* padding: 5px; */
  font-size: .5em;
  background: #EEE;
}
.service{border-bottom: 1px solid #EEE;}
.item{width: 24mm;}
.itemtext{font-size: .5em;}

#legalcopy{
  margin-top: 5mm;
}

@media print {
  #printPageButton {
    display: none;
  }
}
  

    </style>

</head>

<body>
    <div id="invoice-POS">
      <button id="printPageButton" onClick="window.print();window.location='/en/sale/create';">Print</button>
        <center id="top">
          <div class="logo"></div>
          <div class="info"> 
            <h2>{{ $store_name }}</h2>
          </div><!--End Info-->
        </center><!--End InvoiceTop-->
        
        <div id="mid">
          <div class="info">
            <h2>Contact Info</h2>
            <p> 
                Address : {{ $address }}</br>
                Phone   : {{ $phone }}</br>
                Receipt#   : {{ $sale->number_sale }}</br>
            </p>
          </div>
        </div><!--End Invoice Mid-->
        
        <div id="bot">
    
          <div id="table">
              <table>
                  <tr class="tabletitle">
                      <td class="item"><h2>Item</h2></td>
                      <td class="Hours"><h2>Qty</h2></td>
                      <td class="Rate"><h2>Sub Total</h2></td>
                  </tr>
                  @foreach ($product_sales as $product_sale )
                  <tr class="service">
                      <td class="tableitem"><p class="itemtext">{{ $product_sale->product_name }}</p></td>
                      <td class="tableitem"><p class="itemtext">{{ $product_sale->pivot->quantity }}</p></td>
                      <td class="tableitem"><p class="itemtext">{{ number_format($product_sale->pivot->quantity * $product_sale->sale_price, 2) }}</p></td>
                  </tr>
                  @endforeach
                  <tr class="tabletitle">
                      <td></td>
                      <td class="Rate"><h2>Sub Total</h2></td>
                      <td class="payment"><h2>{{ number_format($sale->total,2) }}</h2></td>
                  </tr>

                  <tr class="tabletitle">
                      <td></td>
                      <td class="Rate"><h2>Discount</h2></td>
                      <td class="payment"><h2>{{ number_format($sale->discount,2) }}</h2></td>
                  </tr>
                  <tr class="tabletitle">
                      <td></td>
                      <td class="Rate"><h2>Total</h2></td>
                      <td class="payment"><h2>{{ number_format($sale->total_amount,2) }}</h2></td>
                  </tr>
                  <tr class="tabletitle">
                      <td></td>
                      <td class="Rate"><h2>Paid</h2></td>
                      <td class="payment"><h2>{{ number_format($sale->paid,2) }}</h2></td>
                  </tr>
                  <tr class="tabletitle">
                      <td></td>
                      <td class="Rate"><h2>Return</h2></td>
                      <td class="payment"><h2>{{ number_format($sale->total_amount - $sale->paid,2) }}</h2></td>
                  </tr>

              </table>
          </div><!--End Table-->

          <div id="legalcopy">
              <p class="legal"><strong>Thank you for Shopping!</strong></p>
          </div>

      </div><!--End InvoiceBot-->
      </div><!--End Invoice-->
    {{--  <script type="text/javascript">
        window.addEventListener("load", window.print());

    </script>  --}}
</body>

</html>
